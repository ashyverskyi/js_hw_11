/* 
Теоретичні питання
1. Що таке події в JavaScript і для чого вони використовуються?
   Події в JavaScript - це спосіб взаємодії з користувачем або середовищем браузера. Вони відбуваються відповідно до дій користувача або стану документа і дозволяють виконувати певні дії відповідно до цих подій.

2. Які події миші доступні в JavaScript? Наведіть кілька прикладів.
    click: Відбувається при кліці (натисканні та відпусканні) на елемент мишею.
    mouseover: Відбувається, коли курсор миші наводиться на елемент.
    mouseout: Відбувається, коли курсор миші покидає межі елемента.
    mousedown: Відбувається при натисканні кнопки миші над елементом.
    mouseup: Відбувається при відпусканні кнопки миші над елементом.

3. Що таке подія "contextmenu" і як вона використовується для контекстного меню?
   Подія "contextmenu" відбувається, коли користувач натискає праву кнопку миші над елементом, що викликає контекстне меню. Ця подія дозволяє вам встановити власне контекстне меню для елементів на вашій сторінці веб-сайту та визначити дії, які відбудуться при відображенні цього меню.



Практичні завдання
 1. Додати новий абзац по кліку на кнопку:
  По кліку на кнопку <button id="btn-click">Click Me</button>, створіть новий елемент <p> з текстом "New Paragraph" і додайте його до розділу <section id="content">
 
 2. Додати новий елемент форми із атрибутами:
 Створіть кнопку з id "btn-input-create", додайте її на сторінку в section перед footer.
    По кліку на створену кнопку, створіть новий елемент <input> і додайте до нього власні атрибути, наприклад, type, placeholder, і name. та додайте його під кнопкою.
 */

// TASK 1

document.getElementById('btn-click').addEventListener('click', function () {
    let content = document.getElementById('content');
    let paragraph = content.querySelector('p');
    if (paragraph) {
        paragraph.remove();
    } else {
        paragraph = document.createElement('p');
        paragraph.innerHTML = 'New Paragraph';
        content.appendChild(paragraph);
    }
});

// TASK 2

let buttonCreate = document.createElement('button');
buttonCreate.id = 'btn-input-create';
buttonCreate.innerHTML = 'Add Input';
buttonCreate.style.display = 'block';
buttonCreate.style.margin = '0 auto';
document.querySelector('footer').before(buttonCreate);

buttonCreate.addEventListener('click', function () {
    let input = document.createElement('input');
    input.type = 'text';
    input.placeholder = 'Enter text here';
    input.name = 'new-input';
    input.style.display = 'block';
    input.style.margin = '10px auto';
    document.getElementById('btn-input-create').append(input);
});





